import RPi.GPIO as GPIO
import board
import busio
import adafruit_ads1x15.ads1115 as ADS
from adafruit_ads1x15.analog_in import AnalogIn
import time

RELAY_PIN = 17 #gpio pin 17 on the raspberry pi will be the relay toggle
WATER_ON_TIME = 30
ONE_HOUR = 10
SLEEP_NUM_HOURS = 20
SLEEP_TIME = SLEEP_NUM_HOURS * ONE_HOUR
THRESHOLD_VOLTAGE = 0.99
SLEEP_INCREMENT = 0.125

def waterOff():
    GPIO.output(RELAY_PIN, GPIO.HIGH) #turn on relay (active low relay)


def waterOn():
    GPIO.output(RELAY_PIN, GPIO.LOW)

GPIO.setmode(GPIO.BCM)

GPIO.setup(RELAY_PIN, GPIO.OUT) #set pin to be output

waterOff()

i2c=busio.I2C(board.SCL, board.SDA)  
ads = ADS.ADS1115(i2c)
chan = AnalogIn(ads, ADS.P0)


print("************************************************ \n i2c connection established with pi \n************************************************")

print("\n\n\n")

print("************************************************ \n Now collecting moisture readings \n************************************************")

while(2<3):
    average = 0
    num_measurements = 0
    
    # Take measurements for an hour
    for x in range (0, int(ONE_HOUR / SLEEP_INCREMENT)):
        try:
            average = average * (num_measurements / (num_measurements + 1)) + chan.voltage * (1 / (num_measurements + 1))
            print(average)
            num_measurements += 1
            
            time.sleep(SLEEP_INCREMENT)
        except:
            time.sleep(SLEEP_INCREMENT)
        
    if (average < THRESHOLD_VOLTAGE):
        print("************************************************ \n The plant has detected to be dry, starting watering \n************************************************")
        waterOn()
        # Water for a fixed time
        time.sleep(WATER_ON_TIME)
        waterOff()
        print("************************************************ \n System is now in sleep mode for ", SLEEP_NUM_HOURS," hours \n************************************************")
        # Disable watering for many hours
        time.sleep(SLEEP_TIME)
    else:
        print("************************************************ \n The plant has detected to be sufficiently moist \n************************************************")

