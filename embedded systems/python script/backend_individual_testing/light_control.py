import RPi.GPIO as GPIO
import board
import busio
import time

LIGHT_PIN = 27 #gpio pin 27 on the raspberry pi will be the light toggle
OFF_TIME = 10 


def lightOff():
    GPIO.output(LIGHT_PIN, GPIO.HIGH) #turn on relay (active low relay)


def lightOn():
    GPIO.output(LIGHT_PIN, GPIO.LOW)

GPIO.setmode(GPIO.BCM)

GPIO.setup(LIGHT_PIN, GPIO.OUT) #set pin to be output

lightOn()

while(2<3):
    time.sleep(OFF_TIME)
    lightOff()
