import RPi.GPIO as GPIO
import board
import busio
import adafruit_ads1x15.ads1115 as ADS
from adafruit_ads1x15.analog_in import AnalogIn
import time
from datetime import datetime
import pyrebase
import random
import sys
import Adafruit_DHT
import threading
from pyfcm import FCMNotification

WATER_0_PIN = 22  # gpio pin 22 on the raspberry pi will be the relay toggle
WATER_1_PIN = 11  # gpio pin 22 on the raspberry pi will be the relay toggle
WATER_ON_TIME = 5
ONE_HOUR = 10
SLEEP_NUM_HOURS = 10
FERTILIZER_SLEEP_NUM_HOURS = 24*7
SECONDS_HOUR = 60*60
SLEEP_TIME = SLEEP_NUM_HOURS * SECONDS_HOUR
FERTILIZER_SLEEP_TIME = FERTILIZER_SLEEP_NUM_HOURS * SECONDS_HOUR
THRESHOLD_VOLTAGE = 0.99
SLEEP_INCREMENT = 0.125
LIGHT_PIN = 27  # gpio pin 27 on the raspberry pi will be the light toggle
FERTILIZER_0_PIN = 17
HUMIDIFIER_PIN = 10
HUMIDITY_THRESHOLD = 60
LIGHT_1_PIN = 9


config = {
    "apiKey": "YHLdhGDWgf7O3E8YX0z9fBacCr14t5TQ2wcfccCP",
    "authDomain": "agms-capston.firebaseapp.com",
    "databaseURL": "https://agms-capston-default-rtdb.firebaseio.com/",
    "storageBucket": "agms-capston.appspot.com"
}

firebase = pyrebase.initialize_app(config)
database = firebase.database()
push_service = FCMNotification(
    api_key="AAAAN0wIq-k:APA91bF4iEDiNfdObBHWIc8lHKON0pAZnA_Hy17AVfh9psqJNezzHurQ5p5yspuxSgdcWcq76T2AkaYqkiiy-8mbfhYF_u1lNUGjbwAiJJpp5E7YxsxZcyZ3fSKbYSmQaYZcqokV7znN")

GPIO.setmode(GPIO.BCM)

i2c = busio.I2C(board.SCL, board.SDA)
ads = ADS.ADS1115(i2c)

humidity_increasing = 0
local_water_level = 100

messageToSend = []


def refill_water():
    global local_water_level
    local_water_level = 100


def waterOff():
    GPIO.output(WATER_0_PIN, GPIO.HIGH)  # turn on relay (active low relay)


def waterOn():
    GPIO.output(WATER_0_PIN, GPIO.LOW)


def water_1_Off():
    GPIO.output(WATER_1_PIN, GPIO.HIGH)  # turn on relay (active low relay)


def water_1_On():
    GPIO.output(WATER_1_PIN, GPIO.LOW)


def fertilizer_0_Off():
    GPIO.setup(FERTILIZER_0_PIN, GPIO.OUT)  # set pin to be output
    # turn on relay (active low relay)
    GPIO.output(FERTILIZER_0_PIN, GPIO.HIGH)


def fertilizer_0_On():
    GPIO.setup(FERTILIZER_0_PIN, GPIO.OUT)  # set pin to be output
    GPIO.output(FERTILIZER_0_PIN, GPIO.LOW)


def humidifierOff():
    GPIO.setup(HUMIDIFIER_PIN, GPIO.OUT)  # set pin to be output
    GPIO.output(HUMIDIFIER_PIN, GPIO.HIGH)  # turn on relay (active low relay)


def humidifierOn():
    GPIO.setup(HUMIDIFIER_PIN, GPIO.OUT)  # set pin to be output
    GPIO.output(HUMIDIFIER_PIN, GPIO.LOW)


def lightOn():
    GPIO.setup(LIGHT_PIN, GPIO.OUT)  # set pin to be output
    GPIO.output(LIGHT_PIN, GPIO.LOW)
    
    return time.time()

def lightOn1():
    #turn on light on shelf 2
    GPIO.setup(LIGHT_1_PIN, GPIO.OUT)
    GPIO.setup(LIGHT_1_PIN, GPIO.OUT)
    
    return time.time()


def lightOff():
    GPIO.setup(LIGHT_PIN, GPIO.OUT)  # set pin to be output
    GPIO.output(LIGHT_PIN, GPIO.HIGH)  # turn on relay (active low relay)
    return time.time()

def lightOff1():
    GPIO.setup(LIGHT_1_PIN, GPIO.OUT)
    GPIO.output(LIGHT_1_PIN, GPIO.OUT)
    
    return time.time()


def fertilizerSystem():
    fertilizer_0_On()
    time.sleep(WATER_ON_TIME)
    fertilizer_0_Off()
    return time.time()


def collectMoistureData():
    average = 0
    for x in range(0, int(ONE_HOUR / SLEEP_INCREMENT)):
        try:
            average = average * (num_measurements / (num_measurements + 1)) + \
                chan.voltage * (1 / (num_measurements + 1))
            print(average)
            num_measurements += 0.1

            time.sleep(SLEEP_INCREMENT)
        except:
            time.sleep(SLEEP_INCREMENT)

    return average


def waterSystem():
    global local_water_level
    chan = AnalogIn(ads, ADS.P0)
    average = 0
    num_measurements = 0
   # test soil moisture sensor data before powering pump or not
    GPIO.setup(WATER_0_PIN, GPIO.OUT)  # set pin to be output
    GPIO.setup(WATER_1_PIN, GPIO.OUT)
    test2 = database.child("water_setting").update({"waterOn": True})

    average = collectMoistureData()

    if (average < database.child("moisture_setting").child("value").get().val()):
        print("************************************************ \n The plant has detected to be dry, starting watering \n************************************************")

        waterOn()
        # Water for a fixed time
        time.sleep(WATER_ON_TIME)
        local_water_level -= 5
        waterOff()
        LastWatering = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        data_water = {"last_watering": LastWatering}
        postwater = database.child("water").update(data_water)

        print("Message successfully sent")
        print("last watering shelf 1 at: ", LastWatering)
        print("************************************************ \n System shelf 0 is now in sleep mode for ",
              SLEEP_NUM_HOURS, " hours \n************************************************")
    else:
        print("************************************************ \n The plant has detected to be sufficiently moist \n************************************************")
    
    
    test2 = database.child("water_setting").update({"waterOn": False})
    
    #shelf 2 
    chan = AnalogIn(ads, ADS.P3)
    average = collectMoistureData()
    # insert database code for lower shelf moisture readings
    if (average < database.child("moisture_setting1").child("value").get().val()):
        print("************************************************ \n The plant has detected to be dry, starting watering \n************************************************")

        water_1_On()
        # Water for a fixed time
        time.sleep(WATER_ON_TIME)
        local_water_level -= 5
        water_1_Off()
        
        LastWatering = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        data_water = {"last_watering": LastWatering}
        postwater = database.child("water1").update(data_water)
        
        #update database with water pump
        print("Message successfully sent")
        print("last watering shelf 2 at: ", LastWatering)
        
        print("************************************************ \n System shelf 1 is now in sleep mode for ",
              SLEEP_NUM_HOURS, " hours \n************************************************")
    else:
        print("************************************************ \n The plant has detected to be sufficiently moist \n************************************************")
    
    # to turn on lower shelf water: water_1_On(), water_1_Off()
    
    return time.time()  # reset timer


def remoteControlSystem(data_time):
    flagWatering = False  # for watering to not push data every ms
    flagLighting = False  # for lighting to not push data every ms
    while(database.child("automation").child("automation").get().val() == False):
        # lighting remote control
        if (database.child("automation").child("lightOn").get().val() == True and flagLighting == False):
            GPIO.setup(LIGHT_PIN, GPIO.OUT)  # set pin to be output
            lightOn()
            print("light is on")
            flagLighting = True
        elif(database.child("automation").child("lightOn").get().val() == False):
            GPIO.setup(LIGHT_PIN, GPIO.OUT)  # set pin to be output
            lightOff()
            flagLighting = False
        
        # lighting remote control
        if (database.child("automation").child("lightOn1").get().val() == True and flagLighting == False):
            GPIO.setup(LIGHT_1_PIN, GPIO.OUT)  # set pin to be output
            lightOn1()
            print("light is on")
            flagLighting = True
        elif(database.child("automation").child("lightOn1").get().val() == False):
            GPIO.setup(LIGHT_1_PIN, GPIO.OUT)  # set pin to be output
            lightOff1()
            flagLighting = False

        # watering remote control
        if (database.child("automation").child("waterOn").get().val() == True and flagWatering == False):
            print("water is on")
            waterOn()
            LastWatering = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            data_water = {"last_watering": LastWatering}
            postwater = database.child("water").update(data_water)
            flagWatering = True
        elif(database.child("automation").child("waterOn").get().val() == False):
            GPIO.setup(WATER_0_PIN, GPIO.OUT)  # set pin to be output
            waterOff()
            flagWatering = False
            
        # watering remote control
        if (database.child("automation").child("waterOn1").get().val() == True and flagWatering == False):
            print("water is on")
            water_1_On()
            LastWatering = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            data_water = {"last_watering": LastWatering}
            postwater = database.child("water1").update(data_water)
            flagWatering = True
        elif(database.child("automation").child("waterOn1").get().val() == False):
            GPIO.setup(WATER_1_PIN, GPIO.OUT)  # set pin to be output
            water_1_Off()
            flagWatering = False

        if(data_time == 0 or (time.time() - data_time) > 1800):
            data_time = sensorDataSystem()

    return data_time


def addDataToFirebase(sensorName, dateTime, sensorValue, shelfNumber):
    # Data for statistics
    currentDailyData = {"value": sensorValue}
    year = dateTime.year
    month = dateTime.month
    dayOfYear = dateTime.timetuple().tm_yday
    weekOfYear = dateTime.isocalendar()[1]
    dayOfWeek = dateTime.weekday() + 1
    hour = dateTime.hour
    weekOfMonth = dateTime.isocalendar(
    )[1] - dateTime.replace(day=1).isocalendar()[1] + 1

    currentDailyData["sum"] = currentDailyData["value"]
    currentDailyData["count"] = 1
    currentDailyData["hour"] = hour

    hourlyData = {"count": 1, "sum": currentDailyData["value"], "hour": hour}
    dailyData = {"count": 1,
                 "sum": currentDailyData["value"], "dayOfWeek": dayOfWeek}
    weeklyData = {"count": 1,
                  "sum": currentDailyData["value"], "weekOfMonth": weekOfMonth}
    monthlyData = {"count": 1,
                   "sum": currentDailyData["value"], "month": month}

    hourlyDataPath = dailyDataPath = weeklyDataPath = monthlyDataPath = ""

    # Creating custom path depending on the type of sensor and when the data was collected
    if shelfNumber == 0:
        hourlyDataPath = "{}_hourly_data_{}/{}/{}".format(
            sensorName, year, dayOfYear, hour)
        dailyDataPath = "{}_daily_data_{}/{}/{}".format(
            sensorName, year, weekOfYear, dayOfWeek)
        weeklyDataPath = "{}_weekly_data_{}/{}/{}".format(
            sensorName, year, month, weekOfMonth)
        monthlyDataPath = "{}_monthly_data_{}/{}".format(
            sensorName, year, month)

    elif shelfNumber == 1:
        hourlyDataPath = "{}_hourly_data1_{}/{}/{}".format(
            sensorName, year, dayOfYear, hour)
        dailyDataPath = "{}_daily_data1_{}/{}/{}".format(
            sensorName, year, weekOfYear, dayOfWeek)
        weeklyDataPath = "{}_weekly_data1_{}/{}/{}".format(
            sensorName, year, month, weekOfMonth)
        monthlyDataPath = "{}_monthly_data1_{}/{}".format(
            sensorName, year, month)

    addData(hourlyDataPath, hourlyData)
    addData(dailyDataPath, dailyData)
    addData(weeklyDataPath, weeklyData)
    addData(monthlyDataPath, monthlyData)


def addData(path, data):
    # Adding or updating statistics
    currentData = data
    retVal = 0
    result = database.child(path).get().val()
    if result != None:
        currentData["count"] += result["count"]
        currentData["sum"] += result["sum"]

    # if it wasnt able to connect before add all the last message
    messageToSend.append([path, data])
    for message in messageToSend:
        retVal = database.child(message[0]).set(message[1])
        messageToSend.remove(message)

    return retVal


def sensorDataSystem():
    global humidity_increasing
    # water level system to implement after midterm
    currentTime = datetime.now()
    datatime = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    # only fake data in this script
    data_wtlv = {"value": 1, "DateStamp": datatime}

    # humidty and temperature data
    humidity, temperature = Adafruit_DHT.read_retry(11, 4)
    data_humidity = {"value": humidity, "DateStamp": datatime}
    data_temp = {"value": temperature, "DateStamp": datatime}

    # second shelf - add to database
    humidity1, temperature1 = Adafruit_DHT.read_retry(11, 14)
    data_humidity1 = {"value": humidity1, "DateStamp": datatime}
    data_temp1 = {"value": temperature1, "DateStamp": datatime}


    if humidity_increasing == 0:
        humidity_increasing = humidity1

    if humidity1 != None and humidity1 < HUMIDITY_THRESHOLD:
        humidifierOn()
        if humidity_increasing <= 60:
            humidity_increasing += 1

    else:
       humidifierOff()

    # gradually increase humidity reading
    humidity1 = humidity_increasing

    #Adding first shelf data to db
    postwaterlevel = database.child("water_level").update(data_wtlv)
    postmoist = database.child("moisture").update(data_humidity)
    posttemp = database.child("temperature").update(data_temp)

    #Adding second shelf data to db
    postmoist1 = database.child("moisture1").update(data_humidity1)
    posttemp1 = database.child("temperature1").update(data_temp1)

    addDataToFirebase("water_level", currentTime, data_wtlv["value"],0)
    addDataToFirebase("temperature", currentTime, data_temp["value"],0)
    addDataToFirebase("moisture", currentTime, data_humidity["value"], 0)
    
    addDataToFirebase("temperature", currentTime, data_temp1["value"],1)
    addDataToFirebase("moisture", currentTime, data_humidity1["value"],1)

    print("Message successfully sent")
    print("last data collected at: ", datatime)
    return time.time()


def set_interval(func, sec):
    def func_wrapper():
        set_interval(func, sec)
        func()
    t = threading.Timer(sec, func_wrapper)
    t.start()
    return t

def sendTemperatureNotification(temperature, tokens, shelfNumber):
    # -------------------------Temperature notification------------------------
    message_title = "Temperature Alert Shelf "+ str(shelfNumber)
    if not (temperature > 22 and temperature < 30):
        if temperature < 22 and temperature > 19:
            message_body = "Attention required: Temperature below recommended level"
        elif temperature < 33 and temperature > 30:
            message_body = "Attention required: Temperature above recommended level"
        else:
            message_title = "Temperature Warning Shelf"+ str(shelfNumber)
            message_body = "Attention required: Temperature at a dangerous level"

        for i in tokens:
            result = push_service.notify_single_device(
                registration_id=i, message_title=message_title, message_body=message_body)

def sendMoistureNotification(moisture, tokens, shelfNumber):
    
# ---------------------------Moisture notification---------------------------
    message_title = "Moisture Alert Shelf "+str(shelfNumber)
    if not (moisture > 35 and moisture < 65):
        if moisture < 35 and moisture > 25:
            message_body = "Attention required: Moisture below recommended level"
        elif moisture < 75 and moisture > 65:
            message_body = "Attention required: Moisture above recommended level"
        else:
            message_title = "Moisture Warning Shelf "+str(shelfNumber)
            message_body = "Attention required: Moisture at a dangerous level"

        for i in tokens:
            result = push_service.notify_single_device(
                registration_id=i, message_title=message_title, message_body=message_body)

def sendWaterLevelNotification(water_level,tokens):
    # -----------------------------Water level notification--------------------------
    message_title = "Water level Warning"
    if water_level < 1:
        if water_level <= 0.12:
            message_body = "Attention required: Water level dangerously low"
        elif water_level < 0.25:
            message_title = "Water level Alert"
            message_body = "Attention required: Water level low"

        for i in tokens:
            result = push_service.notify_single_device(
                registration_id=i, message_title=message_title, message_body=message_body)



def sendNotification():
    result1 = database.child("device_tokens").get()

    tokens = []
    for data in result1.each():
        tokens.append(data.val())

    temperature = database.child("temperature").child("value").get().val()
    moisture = database.child("moisture").child("value").get().val()
    water_level = database.child("water_level").child("value").get().val()

    temperature1 = database.child("temperature1").child("value").get().val()
    moisture1 = database.child("moisture1").child("value").get().val()

    message_title = "Automated Greenhouse Management System"
    message_body = "Here's a short notification and ...."

    sendTemperatureNotification(temperature, tokens, 1)
    sendMoistureNotification(temperature1, tokens, 2)
    sendMoistureNotification(moisture, tokens, 1)
    sendMoistureNotification(moisture1,tokens,2)
    sendWaterLevelNotification(water_level,tokens)




print("************************************************ \n i2c connection established with pi \n************************************************")

print("\n\n\n")

print("************************************************ \n Now collecting data \n************************************************")


def main():
    data_time = 0
    current_time = 0
    fertilizer_time = 0
    light0_time = time.time()
    light1_time = time.time()

    humdifier_timer = 0  # same timer as watering?

    while(True):

        #try:
            # automated light system - timer light_time to measure the lighting every 60 seconds
            if (database.child("lights").child("light_off").get().val() == datetime.now().strftime("%H:%M") and (time.time() - light0_time) > 31):
                light0_time = lightOff()
                print("light is off")
            elif (database.child("lights").child("light_on").get().val() == datetime.now().strftime("%H:%M") and (time.time() - light0_time) > 31):
                light0_time = lightOn()
                print("light is on")

            if (database.child("lights1").child("light_off").get().val() == datetime.now().strftime("%H:%M") and (time.time() - light1_time) > 31):
                light1_time = lightOff1()
                print("light is off")
            elif (database.child("lights1").child("light_on").get().val() == datetime.now().strftime("%H:%M") and (time.time() - light1_time) > 31):
                light1_time = lightOn1()
                print("light is on")

            # automated watering system - timer current_time to measure the watering every 10 hours
            if (database.child("automation").child("automation").get().val() == True and current_time == 0 or (time.time() - current_time) > SLEEP_TIME):
                current_time = waterSystem()
                # humidfierSystem()

            if (database.child("automation").child("automation").get().val() == True and fertilizer_time == 0 or (time.time() - fertilizer_time) > FERTILIZER_SLEEP_TIME):
                fertilizer_time = fertilizerSystem()

            # remote control options
            elif (database.child("automation").child("automation").get().val() == False):
                data_time = remoteControlSystem(data_time)

            # sensor data - timer data_time to measuer the sensors every 30 minutes
            elif(data_time == 0 or (time.time() - data_time) > 1800):
                data_time = sensorDataSystem()
            else:
                time.sleep(3)
        #except:
            #print("connection error retrying")
            #time.sleep(5)


timer = set_interval(sendNotification, 43200)
main()
timer.cancel()
