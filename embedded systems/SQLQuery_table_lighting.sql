-- Create a new table called 'Watering' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.Light', 'U') IS NOT NULL
DROP TABLE dbo.Light
GO
-- Create the table in the specified schema
CREATE TABLE dbo.Light
(
   LightID        INT    NOT NULL   PRIMARY KEY, -- primary key column
   LightOn  TIME NOT NULL, 
   LightOff TIME NOT NULL
);
GO
