-- Create a new table called 'Watering' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.WaterLevel', 'U') IS NOT NULL
DROP TABLE dbo.WaterLevel
GO
-- Create the table in the specified schema
CREATE TABLE dbo.WaterLevel
(
   WaterLevelID        INT    NOT NULL   PRIMARY KEY, -- primary key column
   WaterLevel  FLOAT NOT NULL
);
GO
