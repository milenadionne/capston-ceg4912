-- Create a new table called 'Watering' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.Water', 'U') IS NOT NULL
DROP TABLE dbo.Water
GO
-- Create the table in the specified schema
CREATE TABLE dbo.Water
(
   WaterID        INT    NOT NULL   PRIMARY KEY, -- primary key column
   LastWatering DATETIME NOT NULL
);
GO