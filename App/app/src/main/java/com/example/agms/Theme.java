package com.example.agms;

public class Theme {
    int id;
    String theme_color;

    // constructors
    public Theme() {
    }

    public Theme(String theme_color) {
        this.theme_color = theme_color;
    }

    public Theme(int id, String theme_color) {
        this.id = id;
        this.theme_color = theme_color;
    }

    // setters
    public void setId(int id) {
        this.id = id;
    }

    public void setTheme_color(String theme_color) {
        this.theme_color = theme_color;
    }


    // getters
    public long getId() {
        return this.id;
    }

    public String getTheme_color() {
        return this.theme_color;
    }
}
