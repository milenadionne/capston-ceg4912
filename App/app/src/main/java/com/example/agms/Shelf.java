package com.example.agms;

public class Shelf {
    int id;
    String shelf;

    // constructors
    public Shelf() {
    }

    public Shelf(String theme_color) {
        this.shelf = theme_color;
    }

    public Shelf(int id, String theme_color) {
        this.id = id;
        this.shelf = theme_color;
    }

    // setters
    public void setId(int id) {
        this.id = id;
    }

    public void setShelf(String theme_color) {
        this.shelf = theme_color;
    }


    // getters
    public long getId() {
        return this.id;
    }

    public String getShelf() {
        return this.shelf;
    }
}
