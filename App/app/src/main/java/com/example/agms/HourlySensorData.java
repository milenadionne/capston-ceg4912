package com.example.agms;

public class HourlySensorData {
    private String month;


    private float sum,count,hour;

    public HourlySensorData(){

    }

    public HourlySensorData(String month, float sum, float count, float hour){
        this.month = month;
        this.sum = sum;
        this.count = count;
        this.hour = hour;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public float getSum() {
        return sum;
    }

    public void setSum(float sum) {
        this.sum = sum;
    }

    public float getCount() {
        return count;
    }

    public void setCount(float count) {
        this.count = count;
    }

    public float getHour() {
        return hour;
    }

    public void setHour(float hour) {
        this.hour = hour;
    }


}
