package com.example.agms;
import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;


public class WaterSetting extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    Context context = this;
    String time;
    DatabaseHelper db;
    String styleText = "";
    String value;
    public Spinner spinner;
    public DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.water_setting);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        TextView tv = (TextView) findViewById(R.id.textView2);
        tv.setText(styleText);
        db = new DatabaseHelper(getApplicationContext());

        spinner = (Spinner) findViewById(R.id.spinner);
        final Spinner spinner2 = (Spinner) findViewById(R.id.swicthShelf);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context, R.array.shelfnum, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter);
        spinner2.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) context);

        if (db.getShelf(1).equals("shelf1")){
            adapter = ArrayAdapter.createFromResource(context, R.array.shelfnum, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner2.setAdapter(adapter);
            spinner2.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) context);
        } else {
            adapter = ArrayAdapter.createFromResource(context, R.array.shelfnum2, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner2.setAdapter(adapter);
            spinner2.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) context);
        }

        setSpinner();

    }
    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        // An item was selected. You can retrieve the selected item using
        String value = parent.getItemAtPosition(pos).toString();

        TextView tv = findViewById(R.id.textView2);
        if (value.equals("Low")){
            if (db.getShelf(1).equals("shelf1")) {
                mDatabase.child("moisture_setting").child("value").setValue(1.2);
            } else {
                mDatabase.child("moisture_setting1").child("value").setValue(1.2);
            }
            styleText = "Soil moisture set on low: ideal for dry plants with low maintenance: cactus, coneflower, thyme";
        } else if(value.equals("Medium")){
            if (db.getShelf(1).equals("shelf1")) {
                mDatabase.child("moisture_setting").child("value").setValue(0.99);
            } else {
                mDatabase.child("moisture_setting1").child("value").setValue(0.99);
            }
            styleText = "Soil moisture set on medium: ideal for garden plants with medium maintenance: tomatos, lettuce, grass";
        } else if (value.equals("High")){
            if (db.getShelf(1).equals("shelf1")) {
                mDatabase.child("moisture_setting").child("value").setValue(0.8);
            } else {
                mDatabase.child("moisture_setting1").child("value").setValue(0.8);
            }
            styleText = "Soil moisture set on high: ideal for high maintenance plants: rice";
        }

        if (value.equals("Shelf 1")){
            db.updateShelf(1,"shelf1");
            setSpinner();

        } else if(value.equals("Shelf 2")){
            db.updateShelf(1,"shelf2");
            setSpinner();
        }

        tv.setText(styleText);

    }
    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }

    public void setSpinner(){
        // Create an ArrayAdapter using the string array and a default spinner layout
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String moist_value;
                if (db.getShelf(1).equals("shelf1")) {
                    moist_value = dataSnapshot.child("moisture_setting").child("value").getValue().toString();
                } else {
                    moist_value = dataSnapshot.child("moisture_setting1").child("value").getValue().toString();
                }
                if (moist_value.equals("0.99")){
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context,
                            R.array.moist_array_medium, R.layout.spinner_item);
                    // Specify the layout to use when the list of choices appears
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    // Apply the adapter to the spinner
                    spinner.setAdapter(adapter);
                    spinner.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) context);
                } else if(moist_value.equals("1.2")){
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context,
                            R.array.moist_array_low, R.layout.spinner_item);
                    // Specify the layout to use when the list of choices appears
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    // Apply the adapter to the spinner
                    spinner.setAdapter(adapter);
                    spinner.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) context);
                } else {
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context,
                            R.array.moist_array_high, R.layout.spinner_item);
                    // Specify the layout to use when the list of choices appears
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    // Apply the adapter to the spinner
                    spinner.setAdapter(adapter);
                    spinner.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) context);
                }

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            setResult(WaterSetting.RESULT_OK);
            finish();
        }
        setResult(WaterSetting.RESULT_OK);
        return super.onOptionsItemSelected(item);
    }

}
