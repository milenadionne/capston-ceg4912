package com.example.agms;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Chronometer;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class SecondFragment extends Fragment implements AdapterView.OnItemSelectedListener{
    public DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
    private Chronometer mChrono;
    private static final long START_TIME_IN_MILLIS = 0;
    public static String supplylevel;
    DatabaseHelper db;
    public ImageView imgShelf1;
    public TextView tempMoist;
    public TextView tempText;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        mDatabase.child("automation").child("automation").setValue(false);
        if (MainActivity.check == true){
            return inflater.inflate(R.layout.fragment_second_light, container, false);
        } else {
            return inflater.inflate(R.layout.fragment_second, container, false);
        }

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        mChrono = (Chronometer) view.findViewById(R.id.chronometer);

        alertDialogConnection();


        Switch waterSwitch = (Switch) getView().findViewById(R.id.swicth3);
        waterSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                if (db.getShelf(1).equals("shelf1")) {
                    if (isChecked) {
                        mDatabase.child("automation").child("waterOn").setValue(true);
                        Toast.makeText(getContext(), "Watering plants", Toast.LENGTH_SHORT).show();
                        mChrono.start();
                        mChrono.setBase(SystemClock.elapsedRealtime());
                    } else {
                        mDatabase.child("automation").child("waterOn").setValue(false);
                        mChrono.stop();
                        mChrono.setBase(SystemClock.elapsedRealtime());
                        Toast.makeText(getContext(), "Stop watering plants", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (isChecked) {
                        mDatabase.child("automation").child("waterOn1").setValue(true);
                        Toast.makeText(getContext(), "Watering plants", Toast.LENGTH_SHORT).show();
                        mChrono.start();
                        mChrono.setBase(SystemClock.elapsedRealtime());
                    } else {
                        mDatabase.child("automation").child("waterOn1").setValue(false);
                        mChrono.stop();
                        mChrono.setBase(SystemClock.elapsedRealtime());
                        Toast.makeText(getContext(), "Stop watering plants", Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });

        Switch lightSwitch = (Switch) getView().findViewById(R.id.swicth4);
        lightSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (db.getShelf(1).equals("shelf1")) {
                    if (isChecked) {
                        mDatabase.child("automation").child("lightOn").setValue(true);
                        Toast.makeText(getContext(), "Light is on", Toast.LENGTH_SHORT).show();
                    } else {
                        mDatabase.child("automation").child("lightOn").setValue(false);
                        Toast.makeText(getContext(), "Light is off", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (isChecked) {
                        mDatabase.child("automation").child("lightOn1").setValue(true);
                        Toast.makeText(getContext(), "Light is on", Toast.LENGTH_SHORT).show();
                    } else {
                        mDatabase.child("automation").child("lightOn1").setValue(false);
                        Toast.makeText(getContext(), "Light is off", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        Switch justSwitch = (Switch) view.findViewById(R.id.swicth2);
        justSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                NavHostFragment.findNavController(SecondFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });


        final TextView waterSupText = (TextView)view.findViewById(R.id.textView17);
        tempText = (TextView)view.findViewById(R.id.textView23);
        tempMoist = (TextView)view.findViewById(R.id.textView20);

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                supplylevel = dataSnapshot.child("water_level").child("value").getValue().toString();
                if (MainActivity.check == true) {
                    waterSupText.setText(supplylevel + " L");
                } else {
                    waterSupText.setText(supplylevel + " L");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        setTemperature();
        setMoisture();


        final Spinner spinner = (Spinner) view.findViewById(R.id.swicthShelf);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.shelfnum, R.layout.dark_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        db = new DatabaseHelper(getContext());
        imgShelf1 = (ImageView) view.findViewById(R.id.imageView);
        if (db.getShelf(1).equals("shelf1")){
            imgShelf1.setImageResource(R.drawable.italian_parsley);
            if (db.getTheme(1).equals("dark")){
                adapter = ArrayAdapter.createFromResource(getContext(), R.array.shelfnum, R.layout.dark_spinner_item);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.getBackground().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
                spinner.setAdapter(adapter);
                spinner.setOnItemSelectedListener(this);
            } else {
                adapter = ArrayAdapter.createFromResource(getContext(), R.array.shelfnum, android.R.layout.simple_spinner_item);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);
                spinner.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) this);

            }

        } else {
            imgShelf1.setImageResource(R.drawable.oregano);
            if (db.getTheme(1).equals("dark")){
                adapter = ArrayAdapter.createFromResource(getContext(), R.array.shelfnum2, R.layout.dark_spinner_item);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.getBackground().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
                spinner.setAdapter(adapter);
                spinner.setOnItemSelectedListener(this);
            } else {
                adapter = ArrayAdapter.createFromResource(getContext(), R.array.shelfnum2, android.R.layout.simple_spinner_item);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);
                spinner.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) this);

            }

        }

    }
    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        // An item was selected. You can retrieve the selected item using
        String value = parent.getItemAtPosition(pos).toString();
        if (value.equals("Shelf 1")){
            imgShelf1.setImageResource(R.drawable.italian_parsley);
            db.updateShelf(1,"shelf1");
            setMoisture();
            setTemperature();
        } else if(value.equals("Shelf 2")){
            imgShelf1.setImageResource(R.drawable.oregano);
            db.updateShelf(1,"shelf2");
            setMoisture();
            setTemperature();
        }

    }
    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }

    public void setTemperature(){
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String temp_value;
                if (db.getShelf(1).equals("shelf1")) {
                    temp_value= dataSnapshot.child("temperature").child("value").getValue().toString();
                } else {
                    temp_value= dataSnapshot.child("temperature1").child("value").getValue().toString();
                }

                if (MainActivity.check == true) {
                    tempText.setText(temp_value + " °C");

                    if (Double.parseDouble(temp_value) >22 && Double.parseDouble(temp_value) < 30){
                        tempText.setTextColor(Color.rgb(69,125,55));
                    } else if ((Double.parseDouble(temp_value) < 22 && Double.parseDouble(temp_value)> 19) ||( Double.parseDouble(temp_value) < 33 && Double.parseDouble(temp_value)> 30)){
                        tempText.setTextColor(Color.rgb(161,161,69));
                    } else {
                        tempText.setTextColor(Color.rgb(107,0,0));
                    }
                } else {
                    tempText.setText(temp_value + " °C");

                    if (Double.parseDouble(temp_value) >22 && Double.parseDouble(temp_value) < 30){
                        tempText.setTextColor(Color.GREEN);
                    } else if ((Double.parseDouble(temp_value) < 22 && Double.parseDouble(temp_value)> 19) ||( Double.parseDouble(temp_value) < 33 && Double.parseDouble(temp_value)> 30)){
                        tempText.setTextColor(Color.YELLOW);
                    } else {
                        tempText.setTextColor(Color.RED);
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            } });
    }
    public void setMoisture(){

        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String moist_value;
                if (db.getShelf(1).equals("shelf1")) {
                    moist_value = dataSnapshot.child("moisture").child("value").getValue().toString();
                } else {
                    moist_value = dataSnapshot.child("moisture1").child("value").getValue().toString();
                }
                if (MainActivity.check == true) {
                    tempMoist.setText(moist_value + " %");
                    if (Double.parseDouble(moist_value) >35 && Double.parseDouble(moist_value) < 65){
                        tempMoist.setTextColor(Color.rgb(69,125,55));
                    } else if ((Double.parseDouble(moist_value) < 35 && Double.parseDouble(moist_value)> 25) ||( Double.parseDouble(moist_value) < 75 && Double.parseDouble(moist_value)> 65)){
                        tempMoist.setTextColor(Color.rgb(161,161,69));
                    } else {
                        tempMoist.setTextColor(Color.rgb(107,0,0));
                    }
                } else {
                    tempMoist.setText(moist_value + " %");
                    if (Double.parseDouble(moist_value) >35 && Double.parseDouble(moist_value) < 65){
                        tempMoist.setTextColor(Color.GREEN);
                    } else if ((Double.parseDouble(moist_value) < 35 && Double.parseDouble(moist_value)> 25) ||( Double.parseDouble(moist_value) < 75 && Double.parseDouble(moist_value)> 65)){
                        tempMoist.setTextColor(Color.YELLOW);
                    } else {
                        tempMoist.setTextColor(Color.RED);
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            } });


    }



    private void alertDialogConnection() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setMessage("Connecting to greenhouse system");
        dialog.setTitle("Connection");
        final AlertDialog alertDialog = dialog.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();

        final Handler handler  = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (alertDialog.isShowing()) {
                    alertDialog.dismiss();
                }
            }
        };

        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                handler.removeCallbacks(runnable);
            }
        });

        handler.postDelayed(runnable, 2000);
    }


}