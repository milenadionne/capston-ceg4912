package com.example.agms;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.sql.Time;
import java.time.LocalDate;

import static com.example.agms.MainActivity.getDbTable;

public class FirstFragment extends Fragment implements AdapterView.OnItemSelectedListener{
    String watertime;
    public static String lightOn;
    public static String lightOff;
    public static int lightOnKey;
    public static String supplylevel;
    private boolean continuetopartial;
    DatabaseHelper db;
    public ImageView imgShelf1;
    public TextView waterText;
    public ImageButton lightBut ;
    public ImageButton waterBut ;
    public ImageButton supplyBut;
    public ImageButton moistBut;
    public TextView lightOnText;
    public TextView lightOffText;
    public TextView waterSupText ;
    public TextView tempText ;
    public TextView tempMoist;
    public ImageButton lightHist;
    public ImageButton tempBut;


    public DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        mDatabase.child("automation").child("automation").setValue(true);
        if (MainActivity.check == true){
            return inflater.inflate(R.layout.fragment_first_light, container, false);
        } else {
            return inflater.inflate(R.layout.fragment_first, container, false);
        }
    }


    public void onViewCreated(View view, Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        lightBut = (ImageButton)view.findViewById(R.id.imageButton);
        waterBut = (ImageButton)view.findViewById(R.id.imageButton2);
        supplyBut = (ImageButton)view.findViewById(R.id.imageButton6);
        moistBut = (ImageButton)view.findViewById(R.id.imageButton7);
        lightOnText = (TextView)view.findViewById(R.id.textView5);
        lightOffText = (TextView)view.findViewById(R.id.textView6);
        waterText = (TextView)view.findViewById(R.id.textView8);
        waterSupText = (TextView)view.findViewById(R.id.textView17);
        tempText = (TextView)view.findViewById(R.id.textView23);
        tempMoist = (TextView)view.findViewById(R.id.textView20);
        lightHist = (ImageButton)view.findViewById(R.id.imageButton1);
        tempBut = (ImageButton)view.findViewById(R.id.imageButton8);


        lightBut.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(getActivity().getApplication(), LightSetting.class);
                startActivityForResult(intent, 10001);
            }
        });


        lightHist.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                LocalDate date = LocalDate.now();
                long epochDate = date.toEpochDay();
                Intent intent = new Intent(getActivity().getApplication(), HistoryActivity.class);
                intent.putExtra("date",epochDate);
                intent.putExtra("selectedButton",R.id.day_button);


                intent.putExtra("ScreenName","Light History");
                intent.putExtra("Table",getDbTable(R.id.light_history));
                intent.putExtra("id",R.id.light);
                intent.putExtra("shelfNum",0);
                startActivity(intent);
                startActivityForResult(intent,-1);

            }
        });



        setWatering();

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                supplylevel = dataSnapshot.child("water_level").child("value").getValue().toString();
                if (MainActivity.check == true) {
                    waterSupText.setText(supplylevel + " L");
                } else {
                    waterSupText.setText(supplylevel + " L");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        waterBut.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(getActivity().getApplication(), WaterSetting.class);
                startActivityForResult(intent, 10001);
            }
        });

        supplyBut.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                LocalDate date = LocalDate.now();
                long epochDate = date.toEpochDay();
                Intent intent = new Intent(getContext(), HistoryActivity.class);
                intent.putExtra("date",epochDate);
                intent.putExtra("selectedButton",R.id.day_button);

                intent.putExtra("ScreenName","Water History");
                intent.putExtra("Table",getDbTable(R.id.water_supply));
                intent.putExtra("id",R.id.water);
                intent.putExtra("shelfNum",0);
                startActivity(intent);
                startActivityForResult(intent,-1);

            }
        });

        setTemperature();


        tempBut.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                LocalDate date = LocalDate.now();
                long epochDate = date.toEpochDay();
                Intent intent = new Intent(getContext(), HistoryActivity.class);
                intent.putExtra("date",epochDate);
                intent.putExtra("selectedButton",R.id.day_button);

                intent.putExtra("ScreenName","Temperature History");
                intent.putExtra("Table",getDbTable(R.id.temperature));
                intent.putExtra("id",R.id.temperature);
                intent.putExtra("shelfNum",0);
                startActivity(intent);
                startActivityForResult(intent,-1);

            }
        });

        setMoisture();

        final Switch justSwitch = view.findViewById(R.id.swicth1);

        justSwitch.toggle();
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String userOn = dataSnapshot.child("automation").child("automation").getValue().toString();
                String waterOn = dataSnapshot.child("water_setting").child("waterOn").getValue().toString();

                if (userOn == "false" || waterOn == "true"){
                    justSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                            justSwitch.toggle();
                            alertDialogUserOn();
                        }
                    });
                } else {
                    justSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                            alertDialog();
                            justSwitch.toggle();
                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        final Spinner spinner = (Spinner) view.findViewById(R.id.swicthShelf);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.shelfnum, R.layout.dark_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        db = new DatabaseHelper(getContext());
        imgShelf1 = (ImageView) view.findViewById(R.id.imageView);
        if (db.getShelf(1).equals("shelf1")){
            imgShelf1.setImageResource(R.drawable.italian_parsley);
            if (db.getTheme(1).equals("dark")){
                adapter = ArrayAdapter.createFromResource(getContext(), R.array.shelfnum, R.layout.dark_spinner_item);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.getBackground().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
                spinner.setAdapter(adapter);
                spinner.setOnItemSelectedListener(this);
            } else {
                adapter = ArrayAdapter.createFromResource(getContext(), R.array.shelfnum, android.R.layout.simple_spinner_item);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);
                spinner.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) this);

            }

        } else {
            imgShelf1.setImageResource(R.drawable.oregano);
            if (db.getTheme(1).equals("dark")){
                adapter = ArrayAdapter.createFromResource(getContext(), R.array.shelfnum2, R.layout.dark_spinner_item);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.getBackground().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
                spinner.setAdapter(adapter);
                spinner.setOnItemSelectedListener(this);
            } else {
                adapter = ArrayAdapter.createFromResource(getContext(), R.array.shelfnum2, android.R.layout.simple_spinner_item);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);
                spinner.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) this);

            }

        }




        moistBut.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                LocalDate date = LocalDate.now();
                long epochDate = date.toEpochDay();
                Intent intent = new Intent(getContext(), HistoryActivity.class);
                intent.putExtra("date",epochDate);
                intent.putExtra("selectedButton",R.id.day_button);

                intent.putExtra("ScreenName","Humidity History");
                intent.putExtra("Table",getDbTable(R.id.humidity));
                intent.putExtra("id",R.id.humidity);
                intent.putExtra("shelfNum",0);
                startActivity(intent);
                startActivityForResult(intent,-1);

            }
        });



    }
    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        // An item was selected. You can retrieve the selected item using

        String value = parent.getItemAtPosition(pos).toString();
        if (value.equals("Shelf 1")){
            imgShelf1.setImageResource(R.drawable.italian_parsley);
            db.updateShelf(1,"shelf1");
            setMoisture();
            setWatering();
            setTemperature();
            setLight();
        } else if(value.equals("Shelf 2")){
            imgShelf1.setImageResource(R.drawable.oregano);
            db.updateShelf(1,"shelf2");
            setMoisture();
            setWatering();
            setTemperature();
            setLight();
        }
    }
    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }

    public void setLight() {
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (db.getShelf(1).equals("shelf1")) {
                    lightOn = dataSnapshot.child("lights").child("light_on").getValue().toString();
                    lightOff = dataSnapshot.child("lights").child("light_off").getValue().toString();
                } else {
                    lightOn = dataSnapshot.child("lights1").child("light_on").getValue().toString();
                    lightOff = dataSnapshot.child("lights1").child("light_off").getValue().toString();
                }

                if (MainActivity.check == true){

                    lightOnText.setText("Opens at: " + lightOn);
                    lightOffText.setText("Closes at: " + lightOff);
                } else {
                    lightOnText.setText("Opens at: " + lightOn);
                    lightOffText.setText("Closes at: " + lightOff);

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public void setWatering(){
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String lastwatering;
                if (db.getShelf(1).equals("shelf1")) {
                    lastwatering = dataSnapshot.child("water").child("last_watering").getValue().toString();
                } else {
                    lastwatering = dataSnapshot.child("water1").child("last_watering").getValue().toString();
                }
                if (MainActivity.check == true) {
                    waterText.setText(lastwatering);
                } else {
                    waterText.setText(lastwatering);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public void setTemperature(){
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String temp_value;
                if (db.getShelf(1).equals("shelf1")) {
                    temp_value= dataSnapshot.child("temperature").child("value").getValue().toString();
                } else {
                    temp_value= dataSnapshot.child("temperature1").child("value").getValue().toString();
                }

                if (MainActivity.check == true) {
                    tempText.setText(temp_value + " °C");

                    if (Double.parseDouble(temp_value) >22 && Double.parseDouble(temp_value) < 30){
                        tempText.setTextColor(Color.rgb(69,125,55));
                    } else if ((Double.parseDouble(temp_value) < 22 && Double.parseDouble(temp_value)> 19) ||( Double.parseDouble(temp_value) < 33 && Double.parseDouble(temp_value)> 30)){
                        tempText.setTextColor(Color.rgb(161,161,69));
                    } else {
                        tempText.setTextColor(Color.rgb(107,0,0));
                    }
                } else {
                    tempText.setText(temp_value + " °C");

                    if (Double.parseDouble(temp_value) >22 && Double.parseDouble(temp_value) < 30){
                        tempText.setTextColor(Color.GREEN);
                    } else if ((Double.parseDouble(temp_value) < 22 && Double.parseDouble(temp_value)> 19) ||( Double.parseDouble(temp_value) < 33 && Double.parseDouble(temp_value)> 30)){
                        tempText.setTextColor(Color.YELLOW);
                    } else {
                        tempText.setTextColor(Color.RED);
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            } });


    }
    public void setMoisture(){

        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String moist_value;
                if (db.getShelf(1).equals("shelf1")) {
                    moist_value= dataSnapshot.child("moisture").child("value").getValue().toString();
                } else {
                    moist_value= dataSnapshot.child("moisture1").child("value").getValue().toString();
                }

                if (MainActivity.check == true) {
                    tempMoist.setText(moist_value + " %");

                    if (Double.parseDouble(moist_value) >35 && Double.parseDouble(moist_value) < 65){
                        tempMoist.setTextColor(Color.rgb(69,125,55));
                    } else if ((Double.parseDouble(moist_value) < 35 && Double.parseDouble(moist_value)> 25) ||( Double.parseDouble(moist_value) < 75 && Double.parseDouble(moist_value)> 65)){
                        tempMoist.setTextColor(Color.rgb(161,161,69));
                    } else {
                        tempMoist.setTextColor(Color.rgb(107,0,0));
                    }
                } else {
                    tempMoist.setText(moist_value + " %");
                    if (Double.parseDouble(moist_value) >35 && Double.parseDouble(moist_value) < 65){
                        tempMoist.setTextColor(Color.GREEN);
                    } else if ((Double.parseDouble(moist_value) < 35 && Double.parseDouble(moist_value)> 25) ||( Double.parseDouble(moist_value) < 75 && Double.parseDouble(moist_value)> 65)){
                        tempMoist.setTextColor(Color.YELLOW);
                    } else {
                        tempMoist.setTextColor(Color.RED);
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            } });


    }



    private void alertDialogUserOn() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setMessage("Can't connect to partial automation page for one of the following reason: " + "\n\n" +
                "1 - There is already a user on partial automation page, tell the user to disconnect before continuing. " + "\n\n" +
                "2 - Watering system is currently on, try again in 30 seconds");
        dialog.setTitle("Can't connect to partial automation");
        dialog.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });

        AlertDialog alertDialog = dialog.create();
        alertDialog.show();
    }

    private void alertDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setMessage("If you click CONTINUE, you will be bring to the partial automation page. The watering and lighting of the greenhouse won't be taken automatically. Do you want to continue?");
        dialog.setTitle("Partial automation warning");
        dialog.setPositiveButton("CONTINUE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                NavHostFragment.findNavController(FirstFragment.this)
                        .navigate(R.id.action_FirstFragment_to_SecondFragment);
            }
        });
        dialog.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        AlertDialog alertDialog = dialog.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();

    }

    public String getlightOn(){
        String z = "";
        Time time = null;
        final String[] strTime = new String[1];

        mDatabase.child("lights").child("light_on").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                strTime[0] = dataSnapshot.getValue().toString();
                Log.d("light times", strTime[0]);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        return strTime[0];
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);


            super.onActivityResult(requestCode, resultCode, data);
            if ((resultCode == LightSetting.RESULT_OK))
                // recreate your fragment here
                getFragmentManager().beginTransaction().detach(this).attach(this).commit();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }
}