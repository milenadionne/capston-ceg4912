

package com.example.agms;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SettingFragment extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private Spinner spinner;
    private Switch justSwitch;
    private Button helpBut;
    private Button addBut;

    public DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
    Context context = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final DatabaseHelper db = new DatabaseHelper(getApplicationContext());

        setContentView(R.layout.setting_fragment);

        spinner = (Spinner) findViewById(R.id.spinner1);
        justSwitch = findViewById(R.id.switch1);
        helpBut = (Button)findViewById(R.id.button2);
        addBut = (Button)findViewById(R.id.butPlus);

        setSpinner();

        helpBut.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                helpDialog();
            }
        });

        addBut.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                addDialog(v);
            }
        });

        if (db.getTheme(1).equals("light")){
            justSwitch.setText("light theme");
            LinearLayout rl = findViewById(R.id.setting_fragment);
            rl.setBackgroundColor(Color.WHITE);
        } else {
            justSwitch.setText("dark theme");
            justSwitch.setTextColor(Color.WHITE);
            LinearLayout rl = findViewById(R.id.setting_fragment);
            rl.setBackgroundColor(Color.rgb(
                    70, 77, 69
            ));

            justSwitch.toggle();
        }
        justSwitch.toggle();
        justSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    justSwitch.setText("dark theme");
                    justSwitch.setTextColor(Color.WHITE);
                    db.updateTheme(1,"dark");
                    LinearLayout rl = findViewById(R.id.setting_fragment);
                    rl.setBackgroundColor(Color.rgb(
                            70, 77, 69
                    ));

                    setSpinner();
                } else {
                    justSwitch.setText("light theme");
                    justSwitch.setTextColor(Color.BLACK);
                    db.updateTheme(1,"light");
                    LinearLayout rl = findViewById(R.id.setting_fragment);
                    rl.setBackgroundColor(Color.WHITE);

                    setSpinner();
                }
            }
        });

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

    }

    private void addDialog(View view){

        //create a builder
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //set the title
        builder.setTitle("New plant custom");

        //set the view of the dialog
        final View dialogView = getLayoutInflater().inflate(R.layout.nav_add_plant, null);
        builder.setView(dialogView);



        //set the add button
        builder.setPositiveButton("add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });

        //set the cancel button
        builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        //show the dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void setSpinner(){
        final List<String> items = new ArrayList<String>();
        final DatabaseHelper db = new DatabaseHelper(getApplicationContext());
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                DataSnapshot data_value = dataSnapshot.child("plants");
                for (DataSnapshot postSnapshot: data_value.getChildren()){
                    items.add(postSnapshot.getKey());
                }

                if (db.getTheme(1).equals("dark")){
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>  (getBaseContext(), R.layout.dark_spinner_item, items);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.getBackground().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
                    spinner.setAdapter(dataAdapter);
                } else {
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>  (getBaseContext(), android.R.layout.simple_spinner_item, items);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.getBackground().setColorFilter(getResources().getColor(R.color.colorBlack), PorterDuff.Mode.SRC_ATOP);
                    spinner.setAdapter(dataAdapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }


    private void helpDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage("This button will set default values for the soil moisture sensor and the light system depending on your type of plant. " +
                "There is 3 different default settings of plants:" + "\n\n" +
                "1 - High Maintenance plants: soil moisture set at 0.8, light opens at 7AM and closes at 7PM" + "\n\n" +
                "2 - Medium Maintenance plants: soil moisture set at 1, light opens at 7AM and closes at 5PM" + "\n\n" +
                "3 - Low Maintenance plants: soil moisture set at 1.2, light opens at  7AM and closed at 5PM");
        dialog.setTitle("Help");
        dialog.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });

        AlertDialog alertDialog = dialog.create();
        alertDialog.show();
    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {

        String value = parent.getItemAtPosition(pos).toString();

    }
    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getItemId() == android.R.id.home) {
            setResult(SettingFragment.RESULT_OK);
            Intent intent = new Intent(getApplication(), MainActivity.class);
            startActivityForResult(intent, 10001);

        }
        setResult(SettingFragment.RESULT_OK);
        return super.onOptionsItemSelected(item);
    }
}
