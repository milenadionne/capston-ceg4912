package com.example.agms;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.Objects;


public class LightSetting extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    Context context = this;
    String time;
    DatabaseHelper db;
    public DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
    public TextView textView;
    public TextView textView2;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.light_setting);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        db = new DatabaseHelper(getApplicationContext());


        Button button = (Button) findViewById(R.id.button7);
        Button button2 = (Button) findViewById(R.id.button8);
        textView = (TextView) findViewById(R.id.textView9);
        textView2 = (TextView) findViewById(R.id.textView10);

        final Spinner spinner = (Spinner) findViewById(R.id.swicthShelf);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context, R.array.shelfnum, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) context);

        if (db.getShelf(1).equals("shelf1")){
            adapter = ArrayAdapter.createFromResource(context, R.array.shelfnum, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);
            spinner.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) context);
        } else {
            adapter = ArrayAdapter.createFromResource(context, R.array.shelfnum2, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);
            spinner.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) context);
        }

        StringBuffer buffer = new StringBuffer();

        setLights();

        Calendar calendar = Calendar.getInstance();
        final int hour = calendar.get(Calendar.HOUR_OF_DAY);
        final int minute = calendar.get(Calendar.MINUTE);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int i, int i1) {
                        String str1;
                        if (i1 < 10) {
                            str1 = "0" + i1;
                            time = i + ":" + str1;
                        } else {
                            time = i + ":" + i1;
                        }
                        textView.setText(time);
                        String z = "";
                        try {
                            if (db.getShelf(1).equals("shelf1")) {
                                mDatabase.child("lights").child("light_on").setValue(time);
                            } else {
                                mDatabase.child("lights1").child("light_on").setValue(time);
                            }
                        }
                        catch (Exception ex){
                            z = ex.getMessage();
                            Log.d("sql error", z);

                        }

                    }
                },hour, minute,android.text.format.DateFormat.is24HourFormat(context));
                timePickerDialog.show();
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int i, int i1) {
                        String str1;
                        if (i1 < 10) {
                            str1 = "0" + i1;
                            time = i + ":" + str1;
                        } else {
                            time = i + ":" + i1;
                        }
                        textView2.setText(time);
                        String z = "";
                        try {
                            if (db.getShelf(1).equals("shelf1")) {
                                mDatabase.child("lights").child("light_off").setValue(time);
                            } else {
                                mDatabase.child("lights1").child("light_off").setValue(time);
                            }
                        }
                        catch (Exception ex){
                            z = ex.getMessage();
                            Log.d("sql error", z);

                        }
                    }
                },hour, minute,android.text.format.DateFormat.is24HourFormat(context));
                timePickerDialog.show();
            }
        });


    }

    public void showData(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            setResult(LightSetting.RESULT_OK);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void setLights(){
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String lightOn;
                String lightOff;
                if (db.getShelf(1).equals("shelf1")) {
                    lightOn = dataSnapshot.child("lights").child("light_on").getValue().toString();
                    lightOff = dataSnapshot.child("lights").child("light_off").getValue().toString();
                } else {
                    lightOn = dataSnapshot.child("lights1").child("light_on").getValue().toString();
                    lightOff = dataSnapshot.child("lights1").child("light_off").getValue().toString();
                }

                textView.setText(lightOn);
                textView2.setText(lightOff);

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id)  {
        String value = parent.getItemAtPosition(pos).toString();
        if (value.equals("Shelf 1")){
            setLights();
            db.updateShelf(1,"shelf1");

        } else if(value.equals("Shelf 2")){
            db.updateShelf(1,"shelf2");
            setLights();
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}

