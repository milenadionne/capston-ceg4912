package com.example.agms;

public class MonthlySensorData {
    private float sum;
    private float count;
    private float month;

    public MonthlySensorData(){

    }

    public MonthlySensorData(float sum,float count, float month){
        this.sum = sum;
        this.count = count;
        this.month = month;
    }

    public float getSum() {
        return sum;
    }

    public void setSum(float sum) {
        this.sum = sum;
    }

    public float getCount() {
        return count;
    }

    public void setCount(float count) {
        this.count = count;
    }

    public float getMonth() {
        return month;
    }

    public void setMonth(float month) {
        this.month = month;
    }
}
