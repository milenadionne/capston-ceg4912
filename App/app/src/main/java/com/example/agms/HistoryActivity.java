package com.example.agms;

import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.IsoFields;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class HistoryActivity extends AppCompatActivity {
    /**
     * entries: chart entries
     */
    private final List<Entry> entries = new ArrayList<>();

    /**
     * chart: Chart displayed in the history page
     */
    private LineChart chart;
    /**
     * calendarNavigationId: Id of button clicked in navigation bar
     */
    private int calendarNavigationId;

    /**
     * dbTable: Name of db table
     */
    private String dbTable;
    /**
     * currentDate: date the user is actually
     * dateNow: today's date
     */

   private static LocalDate currentDate,dateNow;
    /**
     * textView: textview displaying currentDate
     */
    private TextView textView;

    /**
     * con: database connection manager; defined in MainActivity
     */
 //   public static Connection con = MainActivity.connectionclass();

    //WeekFields weekFields = WeekFields.of(Locale.getDefault());
    XAxis xAxis;
    int themeTextColor;

    private int shelfIndex;


    public DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();

    @Override
    protected void onCreate(Bundle savedInstanceState){
        //randomDataSet();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_layout);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        BottomNavigationView bottomNavigation = findViewById(R.id.bottom_navigation);
        int bottomNavigationId = getIntent().getIntExtra("id",0);
        shelfIndex = getIntent().getIntExtra("shelfNum",0);
      //  Spinner spinner = findViewById(R.id.spinner);

        //spinner.setSelection(shelfIndex);
        bottomNavigation.setSelectedItemId(bottomNavigationId);
        bottomNavigation.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
        //Do nothing if button is reselected
        bottomNavigation.setOnNavigationItemReselectedListener(new BottomNavigationView.OnNavigationItemReselectedListener() {
            @Override
            public void onNavigationItemReselected(@NonNull MenuItem item) {

            }
        });


        chart = new LineChart(getBaseContext()); //creating LineChart
        chart.setNoDataText("No data available. "+new String(Character.toChars(0x1f641)));
        chart.getPaint(Chart.PAINT_INFO).setTextSize(Utils.convertDpToPixel(20f));
        xAxis = chart.getXAxis();

        FrameLayout fl = findViewById(R.id.chartContainer);
        fl.addView(chart); //adding chart to container


        textView = findViewById(R.id.dateDisplay);
        long epoch = getIntent().getLongExtra("date",0);
        currentDate = LocalDate.ofEpochDay(epoch);
        dateNow = LocalDate.now();
        themeTextColor = textView.getCurrentTextColor();
        //Set screen name
        String screenName = getIntent().getStringExtra("ScreenName");
        dbTable = getIntent().getStringExtra("Table");
        setTitle(screenName);

        calendarNavigationId = getIntent().getIntExtra("selectedButton",0);
        //Change clicked button's text and background color
        Button button = findViewById(calendarNavigationId);
        button.setBackgroundColor(Color.parseColor("#000000"));
        button.setTextColor(Color.parseColor("#ffffff"));
        textDisplay();// display currentDate
        createChart();
        xAxis.setGridColor(themeTextColor);
        xAxis.setTextColor(themeTextColor);

       // chart.getLineData().setValueTextColor(themeTextColor);
        chart.getAxisLeft().setGridColor(themeTextColor);
        chart.getAxisLeft().setTextColor(themeTextColor);
        chart.getAxisRight().setTextColor(themeTextColor);
        chart.getAxisRight().setGridColor(themeTextColor);


    }

    /**
     * Manage navigation between day, week, month and year
     * @param view: view
     */
    public void chartNavigationButtonClick(View view){
        int buttonId = view.getId();
        Button button = findViewById(buttonId);
        calendarNavigationId = button.getId();
        textDisplay();

        createChart();

        //Get border drawable file
        Drawable border = ContextCompat.getDrawable(this,R.drawable.border);
        Drawable rightBorder = ContextCompat.getDrawable(this,R.drawable.right_border);
        Drawable curvedRightBorder = ContextCompat.getDrawable(this,R.drawable.curved_right_border);
        //find button's parent
        ViewGroup parent = findViewById(R.id.button_bar1);


        //set all button within the parent to same text color and same background color
        for(int i = 0; i< parent.getChildCount();i++){
            Button current = (Button)parent.getChildAt(i);
            current.setBackgroundColor(Color.parseColor("#ffffff"));
            current.setTextColor(Color.parseColor("#000000"));
            if(i <parent.getChildCount() - 1){
                //Add border to the button
                current.setBackground(rightBorder);
            }
            else{
                current.setBackground(curvedRightBorder);
            }

        }

        //Change clicked button's text and background color
        button.setBackgroundColor(Color.parseColor("#000000"));
        button.setTextColor(Color.parseColor("#ffffff"));
        findViewById(R.id.button_bar1).setBackground(border);


    }

    /**
     *  Navigate to previous activity
     * @param item: Item selected in menu
     * @return operation result
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            setResult(HistoryActivity.RESULT_OK);
            finish();
        }
        setResult(HistoryActivity.RESULT_OK);
        return super.onOptionsItemSelected(item);
    }

    /**
     * Change X axis on the chart when displaying weekly data
     */
    private  class WeekValueFormatter extends ValueFormatter  {


        public String getAxisLabel(float value, AxisBase axis) {
            String[] mFormat;
            System.out.println(">>>>>>>>>>.................:Value:" + value);
            if(calendarNavigationId == R.id.week_button){
               mFormat = new String[]{"Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"};

                if(value-1>=mFormat.length ){
                    return null;
                }
                else{
                    return mFormat[Math.round(value)-1];
                }
            }
            else if (calendarNavigationId == R.id.month_button){
               // System.out.println("value "+ value);
                //System.out.println(entries);
               // XAxis xAxis = chart.getXAxis();
                //System.out.println(xAxis.getAxisMaximum());
                mFormat = new String[]{"Week 1","Week 2", "Week 3", "Week 4", "Week 5", "Week 6","Week 7"};
                
               if(value-1>=mFormat.length || value -1 <0){
                   return null;
               }
               else{
                   return mFormat[Math.round(value)-1];
               }
            }
            else if (calendarNavigationId == R.id.year_button){
                // System.out.println("value "+ value);
                //System.out.println(entries);
                // XAxis xAxis = chart.getXAxis();
                //System.out.println(xAxis.getAxisMaximum());
                mFormat = new String[]{"Jan","Feb", "Mar", "Apr", "May", "Jun","Jul","Aug","Sep","Oct","Nov","Dec"};

                if(value-1>=mFormat.length || value -1 <0){
                    return null;
                }
                else{
                    return mFormat[Math.round(value)-1];
                }
            }
            else if (calendarNavigationId == R.id.day_button){
                // System.out.println("value "+ value);
                //System.out.println(entries);
                // XAxis xAxis = chart.getXAxis();
                //System.out.println(xAxis.getAxisMaximum());
                mFormat = new String[]{"12am","1am", "2am", "3am", "4am", "5am","6am","7am","8am",
                        "9am","10am","11am","12pm","1pm","2pm","3pm","4pm","5pm","6pm","7pm"
                        ,"8pm","9pm","10pm","11pm"};

                if(value-1>=mFormat.length || value -1 <0){
                    return null;
                }
                else{
                    return mFormat[Math.round(value)-1];
                }
            }
            else{
                return String.valueOf(Math.round(value));
            }

        }
    }



    /**
     * Fetch data from db and create chart
     */
    public void createChart()  {
       final String query = buildQuery(currentDate,calendarNavigationId);

        ValueEventListener valueEventListener = new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                entries.clear();

                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    if(query.contains("hourly")){
                        HourlySensorData data = postSnapshot.getValue(HourlySensorData.class);
                        assert data != null;
                        entries.add(new Entry(data.getHour(),data.getSum()/data.getCount()));
                    }
                    else if(query.contains("daily")){
                        DailySensorData data = postSnapshot.getValue(DailySensorData.class);
                        assert data != null;
                        entries.add(new Entry(data.getDayOfWeek(),data.getSum()/data.getCount()));
                    }
                    else if(query.contains("weekly")){
                        WeeklySensorData data = postSnapshot.getValue(WeeklySensorData.class);
                        assert data != null;
                        entries.add(new Entry(data.getWeekOfMonth(),data.getSum()/data.getCount()));
                    }
                    else{
                        MonthlySensorData data = postSnapshot.getValue(MonthlySensorData.class);
                        assert data != null;
                        entries.add(new Entry(data.getMonth(),data.getSum()/data.getCount()));
                    }


                }
                chart.invalidate();
                if(!entries.isEmpty()){
                    //chart.invalidate();
                    LineDataSet  dataSet = new LineDataSet(entries, getIntent().getStringExtra("ScreenName"));
                    dataSet.setColor(themeTextColor);
                    dataSet.setValueTextColor(themeTextColor);
                    //dataSet.setCircleColor(themeTextColor);
                    if(!dbTable.equals("testing")){
                        int[] colors = createColorArray(dbTable,entries);
                        dataSet.setColors(colors);

                        Set<Integer> colorSet = createColorSet(colors);
                        int j = 0;
                        int [] setArray = new int [colorSet.size()];
                        for (Integer i: colorSet) {
                            setArray[j++] = i;
                        }

                        LegendEntry[] legendEntries = new LegendEntry [setArray.length];
                        for (int i = 0; i < legendEntries.length; i++) {
                            if(setArray[i] == Color.rgb(0,153,0)){
                                legendEntries[i] = new LegendEntry("Good",
                                        Legend.LegendForm.DEFAULT,10,5,
                                        null,Color.rgb(0,153,0));

                            }
                            else if (setArray[i] == Color.rgb(255,215,0)){
                                legendEntries[i] = new LegendEntry("Attention required",
                                        Legend.LegendForm.DEFAULT,10,5,
                                        null,Color.rgb(255,215,0));

                            }
                            else{
                                legendEntries[i] = new LegendEntry("Bad", Legend.LegendForm.DEFAULT,
                                        10,5,null,Color.rgb(230,0,0));

                            }

                        }
                        chart.getLegend().setCustom(legendEntries);
                    }
                    LineData lineData = new LineData(dataSet);
                    lineData.setValueTextColor(themeTextColor);
                    lineData.setValueTextSize(10);

                    chart.setData(lineData);
                    chart.notifyDataSetChanged();

                    xAxis.setAxisMinimum(1);
                    xAxis.setGranularity(1);
                    xAxis.setValueFormatter(new WeekValueFormatter());
                    /*if (calendarNavigationId == R.id.month_button && !entries.isEmpty()) {// dataSet = new LineDataSet(entries, "Label");
                        // xAxis.setAxisMaximum(12);
                        //xAxis.setValueFormatter(new WeekValueFormatter());

                    } else if (calendarNavigationId == R.id.week_button && !entries.isEmpty()) {// dataSet = new LineDataSet(entries, "Label");
                        xAxis.setAxisMinimum(1);
                       // xAxis.setValueFormatter(new WeekValueFormatter());
                    }*/
                }
                else{

                    //chart.invalidate();
                    chart.clear();
                    //chart.notifyDataSetChanged();


                   //

                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("The read failed: " ,databaseError.getMessage());

            }
        };

        if(query.contains("hourly")){
            mDatabase.child(query).child(String.valueOf(currentDate.getDayOfYear()))
                    .addValueEventListener(valueEventListener);
        }
        else if(query.contains("daily")){
            mDatabase.child(query).child(String.valueOf(currentDate.get(IsoFields.WEEK_OF_WEEK_BASED_YEAR)))
                    .addValueEventListener(valueEventListener);
        }
        else if(query.contains("weekly")){
            mDatabase.child(query).child(String.valueOf(currentDate.getMonthValue()))
                    .addValueEventListener(valueEventListener);
        }
        else{
            mDatabase.child(query).addValueEventListener(valueEventListener);
        }



       /* try {

            Statement stmt = con.createStatement();

            ResultSet rs = stmt.executeQuery(query);

            int value;
            int row = 1;
            int dayOfWeek;
            entries.clear();
            //System.out.println("fetch size "  +buttonText);
            ResultSetMetaData rsmd = rs.getMetaData();

            System.out.println("column name" +rsmd.getColumnCount()+ " ");
            while (rs.next()) {
                value = rs.getInt("Average");
                if(R.id.week_button == calendarNavigationId){
                    dayOfWeek = rs.getInt("DayNum");
                    entries.add(new Entry(dayOfWeek,value));
                }
                else{
                    entries.add(new Entry(row,value));
                    row++;
                }
            }
            chart.invalidate();
            LineDataSet  dataSet = new LineDataSet(entries, "Label");
            dataSet.setColor(Color.parseColor("#000000"));

            LineData LineData = new LineData(dataSet);
            chart.setData(LineData);
           // System.out.println("Size "+ entries.size());


        }
        catch (Exception ex){
          ex.printStackTrace();
        }*/
    }

    /**
     * Performs backward navigation. Can be used to move by days, weeks, months and years
     * @param view: view: view
     */
    public void previous(View view){
        currentDate = decreaseDate(calendarNavigationId,currentDate);
        textDisplay();
        createChart();
        //chartNavigationButtonClick(view);
    }

    /**
     *  Performs forward navigation. Can be used to move by days, weeks, months and years
     * @param view: view
     */
    public void next(View view){
        currentDate = increaseDate(calendarNavigationId,currentDate);
        textDisplay();
        createChart();
        //chartNavigationButtonClick(view);
    }

    /**
     *  Decrease the date given as parameter by days, weeks, months or years depending on the
     *  button currently active in the navigation bar.
     * @param id: Id of the active button on the navigation bar
     * @param date: date of the data being currently displayed in the chart
     * @return date received as parameter decremented by a day, a week, a month or a year
     */
    private LocalDate decreaseDate(int id, LocalDate date){
        LocalDate tempDate = date;
        if (id == R.id.day_button) {
            tempDate = tempDate.minusDays(1);
        } else if (id == R.id.week_button) {
            tempDate = tempDate.minusWeeks(1);
        } else if (id == R.id.month_button) {
            tempDate = tempDate.minusMonths(1);
        } else if (id == R.id.year_button) {
            tempDate = tempDate.minusYears(1);
        }
        return tempDate;
    }

    /**
     * Decrease the date given as parameter by days, weeks, months or years depending on the
     * button currently active in the navigation bar.
     * @param id: Id of the active button on the navigation bar
     * @param date: date of the data being currently displayed in the chart
     * @return date received as parameter incremented by a day, a week, a month or a year
     */
    private LocalDate increaseDate(int id, LocalDate date){
        LocalDate tempDate = date;
        if (id == R.id.day_button) {
            tempDate = tempDate.plusDays(1);
        } else if (id == R.id.week_button) {
            tempDate = tempDate.plusWeeks(1);
        } else if (id == R.id.month_button) {
            tempDate = tempDate.plusMonths(1);
        } else if (id == R.id.year_button) {
            tempDate = tempDate.plusYears(1);
        }
        return tempDate;
    }

    /**
     * Build query to fetch data from the db
     * @param date: date the data we want was collected
     * @param id: Id of the button currently selected in navigation bar. Depending on this button,
     *          we will compute daily, weekly, monthly or yearly averages
     * @return the query needed to fetch what we want
     *
     */
    private String buildQuery(LocalDate date, int id)  {
        String shelf = "";
        if(shelfIndex == 1){
            shelf = "1";
        }
        String query;
        final String [] values = {"hourly","daily","weekly","monthly"};
        int year = date.getYear();
        if(dbTable.equals("testing")){
            year = 2020;
            shelf = "";
        }
        if (id == R.id.day_button) {
            /*query = "select avg(Value) as 'Average' from " + dbTable + " where Year(Activity_Date) = " + date.getYear() +
                    " and month(Activity_Date) = " + date.getMonthValue() +
                    " and day(Activity_Date) = " + date.getDayOfMonth() +
                    " group by datepart(hh,Activity_Date) order by datepart(hh,Activity_Date) ;";*/


            query = dbTable + "_"+values[0]+"_data"+shelf+"_"+year;
            if(dbTable.equals("water_level")){
                query = dbTable + "_"+values[0]+"_data_"+year;
            }

        } else if (id == R.id.week_button) {
           // WeekFields weekFields = WeekFields.of(Locale.getDefault());

           /* query = "select avg(Value) as 'Average', datepart(WEEKDAY,Activity_Date) as 'DayNum' " +
                    "from " + dbTable + " where Year(Activity_Date) = " +
                    date.getYear() + " and datepart(wk,Activity_Date)= " +
                    date.get(weekFields.weekOfYear())
                    + " group by  datepart(WEEKDAY,Activity_Date) order by datepart(WEEKDAY,Activity_Date); ";*/
            //System.out.println("query: " + query);
            query = dbTable + "_"+values[1]+"_data"+shelf+"_"+year;
            if(dbTable.equals("water_level")){
                query = dbTable + "_"+values[1]+"_data_"+year;
            }

        } else if (id == R.id.month_button) {
            /*query = "select avg(Value) as 'Average' from " + dbTable + " where Year(Activity_Date) = " +
                    date.getYear() + " and month(Activity_Date) = " + date.getMonthValue() +
                    " group by datepart(wk,Activity_Date) order by datepart(wk,Activity_Date); ";*/

            query = dbTable + "_"+values[2]+"_data"+shelf+"_"+year;
            if(dbTable.equals("water_level")){
                query = dbTable + "_"+values[2]+"_data_"+year;
            }

        } else  {
            /*query = "select avg(Value) as 'Average' from " + dbTable + " where Year(Activity_Date) = " +
                    date.getYear() +
                    " group by datepart(mm,Activity_Date) order by datepart(mm,Activity_Date);";*/
            query = dbTable + "_"+values[3]+"_data"+shelf+"_"+year;
            if(dbTable.equals("water_level")){
                query = dbTable + "_"+values[3]+"_data_"+year;
            }
        }
       System.out.println(query);
        return query;
    }

    /**
     *  Display the date the data being currently displayed was collected
     */
    private void textDisplay(){
        //System.out.println("button text: "+buttonText);

        if (calendarNavigationId == R.id.day_button) {
            if (currentDate.equals(dateNow)) {
                textView.setText(R.string.today);
            } else if (currentDate.equals(dateNow.minusDays(1))) {
                textView.setText(R.string.yesterday);
            } else {
                textView.setText(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).format(currentDate));
            }
        } else if (calendarNavigationId == R.id.week_button) {
            LocalDate monday = currentDate;
            while (monday.getDayOfWeek() != DayOfWeek.MONDAY) {
                monday = monday.minusDays(1);
            }
            LocalDate sunday = currentDate;
            while (sunday.getDayOfWeek() != DayOfWeek.SUNDAY) {
                sunday = sunday.plusDays(1);
            }
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM-dd");
            String date;
            if (monday.getMonthValue() == sunday.getMonthValue()) {
                date = monday.format(formatter) + " - " + sunday.getDayOfMonth() + ", " +
                        sunday.getYear();
            } else if (monday.getYear() != sunday.getYear()) {
                date = monday.format(formatter) + ", " + monday.getYear() + " - " + sunday.format(formatter) + ", " +
                        sunday.getYear();
            } else {
                date = monday.format(formatter) + " - " + sunday.format(formatter) + ", " + sunday.getYear();
            }
            textView.setText(date);
        } else if (calendarNavigationId == R.id.month_button) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMMM yyyy");
            textView.setText(currentDate.format(formatter));
        } else {
            textView.setText(String.valueOf(currentDate.getYear()));
        }
        textView.setTypeface(null, Typeface.BOLD);
        textView.setTypeface(null, Typeface.ITALIC);


    }
BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
        new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            int itemId = item.getItemId();
            if (itemId == R.id.light) {
                restartActivity(R.id.light, "Light History");
                return true;
            } else if (itemId == R.id.water) {
                restartActivity(R.id.water, "Water History");
                return true;
            } else if (itemId == R.id.temperature) {
                restartActivity(R.id.temperature, "Temperature History");
                return true;
            } else if (itemId == R.id.humidity) {
                restartActivity(R.id.humidity, "Humidity History");
                return true;
            }

            return false;
        }
    };


    /**
     *  Restart activity and load data corresponding to the item selected on the bottom navigation
     *  bar
     * @param id: Id of the item selected on the bottom navigation bar
     * @param screenName: ScreenTitle corresponding to the selected item
     */
    private void restartActivity(int id, String screenName){
        int activityId = id;
        if(id == R.id.light)activityId = R.id.light_history;
        else if (id== R.id.water)activityId = R.id.water_supply;
        getIntent().putExtra("id",id);
        getIntent().putExtra("selectedButton",calendarNavigationId);
        getIntent().putExtra("ScreenName",screenName);
        getIntent().putExtra("Table", MainActivity.getDbTable(activityId));
        getIntent().putExtra("date",currentDate.toEpochDay());
        getIntent().putExtra("shelfNum",shelfIndex);
        startActivity(getIntent());
        finishAffinity();
        overridePendingTransition(0, 0);
    }


    @org.jetbrains.annotations.NotNull
    private int [] createColorArray(String table, List<Entry> entries) {
        int[] colors = new int[entries.size()];
        switch (table) {
            case "moisture":
                for (int i = 0; i < entries.size(); i++) {
                    double value = entries.get(i).getY();
                    if (value > 35 && value < 65) {
                        colors[i] = Color.rgb(0,153,0);

                    } else if ((value < 35 && value > 25) || (value < 75 && value > 65)) {
                        colors[i] = Color.rgb(255,215,0);

                    } else {
                        colors[i] = Color.rgb(230,0,0);

                    }
                }
                break;
            case "temperature":
                for (int i = 0; i < entries.size(); i++) {
                    double value = entries.get(i).getY();
                    if (value > 22 && value < 30) {
                        colors[i] = Color.rgb(0,153,0);

                    } else if ((value < 22 && value > 19) || (value < 33 && value > 30)) {
                        colors[i] = Color.rgb(255,215,0);

                    } else {
                        colors[i] = Color.rgb(230,0,0);

                    }
                }
                break;
            case "water_level":
                for (int i = 0; i < entries.size(); i++) {
                    double value = entries.get(i).getY();
                    if (value < 0.12) {

                        colors[i] = Color.rgb(230,0,0);
                    } else if (value < 0.25) {
                        colors[i] = Color.rgb(255,215,0);

                    } else {

                        colors[i] = Color.rgb(0,153,0);
                    }
                }
                break;
        }
        return colors;
    }

    private Set<Integer> createColorSet(int [] colors){
        Set<Integer> colorSet = new HashSet<>();
        for (int color : colors) {
            colorSet.add(color);
        }
        return colorSet;
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.android_action_bar_spinner_menu, menu);

        MenuItem item = menu.findItem(R.id.spinner);
       final Spinner  spinner = (Spinner) item.getActionView();
        final String [] items = {"Shelf 1", "Shelf 2"};

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);

        //adapter.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_checked);

        spinner.setAdapter(adapter);
        spinner.setSelection(shelfIndex);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(HistoryActivity.this,items[position],Toast.LENGTH_SHORT).show();

                shelfIndex = position;
                createChart();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        return true;
    }

}
