package com.example.agms;

public class WeeklySensorData {
    private float weekOfMonth,sum,count,month;




    public WeeklySensorData(){

    }

    public WeeklySensorData(float weekOfMonth,float sum,float count,float month){
        this.weekOfMonth = weekOfMonth;
        this.sum = sum;
        this.count = count;
        this.month = month;
    }

    public float getWeekOfMonth() {
        return weekOfMonth;
    }

    public void setWeekOfMonth(float weekOfMonth) {
        this.weekOfMonth = weekOfMonth;
    }

    public float getSum() {
        return sum;
    }

    public void setSum(float sum) {
        this.sum = sum;
    }

    public float getCount() {
        return count;
    }

    public void setCount(float count) {
        this.count = count;
    }

    public float getMonth() {
        return month;
    }

    public void setMonth(float month) {
        this.month = month;
    }
}
