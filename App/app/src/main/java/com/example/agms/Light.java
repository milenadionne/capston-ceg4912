package com.example.agms;

public class Light {
    int id;
    String time;

    // constructors
    public Light() {
    }

    public Light(String time) {
        this.time = time;
    }

    public Light(int id, String time) {
        this.id = id;
        this.time = time;
    }

    // setters
    public void setId(int id) {
        this.id = id;
    }

    public void setTime(String time) {
        this.time = time;
    }


    // getters
    public long getId() {
        return this.id;
    }

    public String getTime() {
        return this.time;
    }


}
