package com.example.agms;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;

import java.time.LocalDate;
import java.util.Objects;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    static final int CHILD_DONE=1;
    static public boolean check;
    private SharedPreferences sharedPref;
    DatabaseHelper db;

    private static final String TAG = "MainActivity";
    public DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        db = new DatabaseHelper(getApplicationContext());

        if (db.themeCurrent() == 0){
            db.insertTheme("dark", "1");
        }

        if (db.currentShelf() == 0){
            db.insertShelf("shelf1", "1");
        }

        if (db.getTheme(1).equals("light")){
            check = true;
            setContentView(R.layout.activity_main_light);
        } else {
            check = false;
            setContentView(R.layout.activity_main);
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(this, R.color.translucide)));

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        DrawerLayout drawerIntro = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerIntro.openDrawer(GravityCompat.START);
        drawerIntro.closeDrawer(GravityCompat.START);

        SharedPreferences sharedPref = getSharedPreferences("main_activity", MODE_PRIVATE);

        //Create placeholder data for light and water

        //HistoryActivity.randomDataSet();
       FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            Log.w("Error", "Fetching FCM registration token failed", task.getException());
                            return;
                        }

                        // Get new FCM registration token
                        final String token = task.getResult();
                        ValueEventListener valueEventListener = new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                if(snapshot.getChildrenCount() ==0){
                                    mDatabase.child("device_tokens").push().setValue(token);
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {

                            }
                        };

                        mDatabase.child("device_tokens").orderByValue().equalTo(token)
                                .addListenerForSingleValueEvent(valueEventListener);



                    }
                });
        // [END log_reg_token]


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onBackPressed() { //when the user clicks the back button on his Android
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(getApplication(), SettingFragment.class);
            startActivityForResult(intent, 10001);
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        LocalDate date = LocalDate.now();
        long epochDate = date.toEpochDay();
        Intent intent = new Intent(getApplicationContext(), HistoryActivity.class);
        intent.putExtra("date",epochDate);
        intent.putExtra("selectedButton",R.id.day_button);
        if (id == R.id.light_history) {

            intent.putExtra("ScreenName","Light History");
            intent.putExtra("Table",getDbTable(R.id.light_history));
            intent.putExtra("id",R.id.light);
            intent.putExtra("shelfNum",0);
        }

        else if (id == R.id.water_supply){
            //Intent intent = new Intent(this, HistoryActivity.class);
            intent.putExtra("ScreenName","Water History");
            intent.putExtra("Table",getDbTable(R.id.water_supply));
            intent.putExtra("id",R.id.water);
            intent.putExtra("shelfNum",0);
            //sendDataToActivity(intent);
           // startActivityForResult(intent,RESULT_OK);
        }
        else if(id == R.id.temperature){
            intent.putExtra("ScreenName","Temperature History");
            intent.putExtra("Table",getDbTable(R.id.temperature));
            intent.putExtra("id",R.id.temperature);
            intent.putExtra("shelfNum",0);
        }
        else if(id == R.id.humidity){
            intent.putExtra("ScreenName","Humidity History");
            intent.putExtra("Table",getDbTable(R.id.humidity));
            intent.putExtra("id",R.id.humidity);
            intent.putExtra("shelfNum",0);
        }

        startActivity(intent);


        startActivityForResult(intent,RESULT_OK);


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if ((resultCode == 1)) {

            FirstFragment fragment  = new FirstFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.detach(fragment).attach(fragment).commit();
        }
    }

    public static String getDbTable(int id){
        String result = null;
        if (id == R.id.light_history) {

            result = "testing";

        }

        else if (id == R.id.water_supply){
            result = "water_level";
        }
        else if(id == R.id.temperature){
            result = "temperature";
        }
        else if(id == R.id.humidity){
            result = "moisture";
        }
        return  result;
    }



}

