package com.example.agms;

public class DailySensorData {

    private float sum,count,weekOfYear,dayOfWeek;

    public DailySensorData(){

    }

    public DailySensorData(float weekOfYear,float dayOfWeek,float sum, float count){
        this.weekOfYear = weekOfYear;
        this.dayOfWeek = dayOfWeek;
        this.sum = sum;
        this.count = count;
    }

    public float getWeekOfYear() {
        return weekOfYear;
    }

    public void setWeekOfYear(float weekOfYear) {
        this.weekOfYear = weekOfYear;
    }

    public float getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(float dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public float getSum() {
        return sum;
    }

    public void setSum(float sum) {
        this.sum = sum;
    }

    public float getCount() {
        return count;
    }

    public void setCount(float count) {
        this.count = count;
    }
}
