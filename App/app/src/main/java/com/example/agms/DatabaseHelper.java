package com.example.agms;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "database.db";
    // Database Version
   // private static final int DATABASE_VERSION = 1;
    private static final int DATABASE_VERSION = 3;
    // Logcat tag
    private static final String LOG = "DatabaseHelper";

    //Table name
    public static final String TABLE_TIME = "time_table";
    public static final String TABLE_TIME_OFF = "time_table_off";
    public static final String TABLE_THEME_APP = "App_Theme";
    public static final String TABLE_SETTING = "setting_table";
    public static final String TABLE_SHELF = "shelf_table";

    //database time variables
    public static final String TABLE_ID = "time_id";
    public static final String TABLE_ID_OFF = "time_id";
    public static final String TABLE_ID_THEME = "theme_id";
    public static final String TABLE_ID_SETTING = "setting_id";
    public static final String TABLE_ID_SHELF = "shelf_id";
    public static final String TABLE_NAME = "time_name";
    public static final String TABLE_NAME_OFF = "time_name_off";
    public static final String TABLE_THEME_NAME = "time_name";
    public static final String TABLE_NAME_SETTING = "setting_name";
    public static final String TABLE_NAME_SHELF = "shelf_name";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_TABLE_LIGHT_ON = "CREATE TABLE " + TABLE_TIME + "("
                + TABLE_ID + " INTEGER PRIMARY KEY," + TABLE_NAME + " TEXT)";
        sqLiteDatabase.execSQL(CREATE_TABLE_LIGHT_ON);

        String CREATE_TABLE_LIGHT_OFF = "CREATE TABLE " + TABLE_TIME_OFF + "("
                + TABLE_ID_OFF + " INTEGER PRIMARY KEY," + TABLE_NAME_OFF + " TEXT)";
        sqLiteDatabase.execSQL(CREATE_TABLE_LIGHT_OFF);

        String WATER_HISTORY_TABLE = "CREATE TABLE " + "Water_History " + "("
                + "id" + " INT PRIMARY KEY," + "date" + " DATETIME" + ")";
        sqLiteDatabase.execSQL(WATER_HISTORY_TABLE);

        String APP_THEME_TABLE = "CREATE TABLE " + TABLE_THEME_APP + "("
                + TABLE_ID_THEME + " INTEGER PRIMARY KEY," + TABLE_THEME_NAME + " TEXT)";
        sqLiteDatabase.execSQL(APP_THEME_TABLE);

        String CREATE_TABLE_SHELF = "CREATE TABLE " + TABLE_SHELF + "("
                + TABLE_ID_SHELF + " INTEGER PRIMARY KEY," + TABLE_NAME_SHELF + " TEXT)";
        sqLiteDatabase.execSQL(CREATE_TABLE_SHELF);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_TIME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_TIME_OFF);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + "Water_History");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_THEME_APP);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_SETTING);

        onCreate(sqLiteDatabase);
    }


    //inserting a new Light value to TABLE_TIME
    public void insertDataOn(String time, String id){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues cvUser = new ContentValues();
        cvUser.put(TABLE_NAME, time);
        cvUser.put(TABLE_ID, id);

        //inserting row
        db.insert(TABLE_TIME, id, cvUser);

    }

    //inserting a new theme value to TABLE_THEME_APP
    public void insertTheme(String theme, String id){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues cvUser = new ContentValues();
        cvUser.put(TABLE_THEME_NAME, theme);
        cvUser.put(TABLE_ID_THEME, id);

        //inserting row
        db.insert(TABLE_THEME_APP, id, cvUser);

    }

    public void insertShelf(String shelf, String id){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues cvUser = new ContentValues();
        cvUser.put(TABLE_NAME_SHELF, shelf);
        cvUser.put(TABLE_ID_SHELF, id);

        //inserting row
        db.insert(TABLE_SHELF, id, cvUser);

    }

    //inserting a new Light value to TABLE_TIME_OFF
    public void insertDataOff(String time, String id){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues cvUser = new ContentValues();
        cvUser.put(TABLE_NAME_OFF, time);
        cvUser.put(TABLE_ID_OFF, id);

        //inserting row
        db.insert(TABLE_TIME_OFF, id, cvUser);
    }

    public int countLightsOn(){
        String selectQuery = "SELECT  * FROM " + TABLE_TIME;

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        int count = 0;
        if (c.moveToFirst()) {
            do {
                count++;
            } while (c.moveToNext());
        }

        return count;
    }

    public int themeCurrent(){
        String selectQuery = "SELECT  * FROM " + TABLE_THEME_APP;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        int count = 0;
        if (c.moveToFirst()) {
            do {
                count++;
            } while (c.moveToNext());
        }

        return count;
    }

    public int currentShelf(){
        String selectQuery = "SELECT  * FROM " + TABLE_SHELF;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        int count = 0;
        if (c.moveToFirst()) {
            do {
                count++;
            } while (c.moveToNext());
        }

        return count;
    }

    public int defaultSetting(){
        String selectQuery = "SELECT  * FROM " + TABLE_THEME_APP;

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        int count = 0;
        if (c.moveToFirst()) {
            do {
                count++;
            } while (c.moveToNext());
        }

        return count;
    }

    public int countLightsOff(){
        String selectQuery = "SELECT  * FROM " + TABLE_TIME_OFF;

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        int count = 0;
        if (c.moveToFirst()) {
            do {
                count++;
            } while (c.moveToNext());
        }

        return count;
    }

    /*
     * get single theme
     */
    public String getTheme(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TABLE_THEME_APP + " WHERE "
                + TABLE_ID_THEME + " = " + id;

        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null)
            c.moveToFirst();

        Theme td = new Theme();
        td.setId(c.getInt(c.getColumnIndex(TABLE_ID_THEME)));
        td.setTheme_color((c.getString(c.getColumnIndex(TABLE_THEME_NAME))));

        return td.getTheme_color();
    }

    public String getShelf(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TABLE_SHELF + " WHERE "
                + TABLE_ID_SHELF + " = " + id;

        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null)
            c.moveToFirst();

        Shelf td = new Shelf();
        td.setId(c.getInt(c.getColumnIndex(TABLE_ID_SHELF)));
        td.setShelf((c.getString(c.getColumnIndex(TABLE_NAME_SHELF))));

        return td.getShelf();
    }

    /*
     * get single light_on
     */
    public String getLightOn(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TABLE_TIME + " WHERE "
                + TABLE_ID + " = " + id;

        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null)
            c.moveToFirst();

        Light td = new Light();
        td.setId(c.getInt(c.getColumnIndex(TABLE_ID)));
        td.setTime((c.getString(c.getColumnIndex(TABLE_NAME))));

        return td.getTime();
    }

    /*
     * get single light_off
     */
    public String getLightOff(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TABLE_TIME_OFF + " WHERE "
                + TABLE_ID_OFF + " = " + id;

        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null)
            c.moveToFirst();

        Light td = new Light();
        td.setId(c.getInt(c.getColumnIndex(TABLE_ID_OFF)));
        td.setTime((c.getString(c.getColumnIndex(TABLE_NAME_OFF))));

        return td.getTime();
    }

    /*
     * Updating theme
     */
    public int updateTheme(int id, String theme) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(TABLE_THEME_NAME, theme);

        // updating row
        return db.update(TABLE_THEME_APP, values, TABLE_ID_THEME + " = ?",
                new String[] { String.valueOf(id) });
    }

    /*
     * Updating shelf
     */
    public int updateShelf(int id, String shelf) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(TABLE_NAME_SHELF, shelf);

        // updating row
        return db.update(TABLE_SHELF, values, TABLE_ID_SHELF + " = ?",
                new String[] { String.valueOf(id) });
    }

    /*
     * Updating a light_on
     */
    public int updateLightOn(int id, String time) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(TABLE_NAME, time);

        // updating row
        return db.update(TABLE_TIME, values, TABLE_ID + " = ?",
                new String[] { String.valueOf(id) });
    }

    /*
     * Updating a light_on
     */
    public int updateLightOff(int id, String time) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(TABLE_NAME_OFF, time);

        // updating row
        return db.update(TABLE_TIME_OFF, values, TABLE_ID_OFF + " = ?",
                new String[] { String.valueOf(id) });
    }


    /**
     * getting all lights_on
     * */
    public List<Light> getAllLightsOn() {
        List<Light> light = new ArrayList<Light>();
        String selectQuery = "SELECT  * FROM " + TABLE_TIME;

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                Light td = new Light();
                td.setId(c.getInt((c.getColumnIndex(TABLE_ID))));
                td.setTime((c.getString(c.getColumnIndex(TABLE_NAME))));

                // adding to todo list
                light.add(td);
            } while (c.moveToNext());
        }

        return light;
    }

    /**
     * getting all lights_off
     * */
    public List<Light> getAllLightsOff() {
        List<Light> light = new ArrayList<Light>();
        String selectQuery = "SELECT  * FROM " + TABLE_TIME_OFF;

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                Light td = new Light();
                td.setId(c.getInt((c.getColumnIndex(TABLE_ID_OFF))));
                td.setTime((c.getString(c.getColumnIndex(TABLE_NAME_OFF))));

                // adding to todo list
                light.add(td);
            } while (c.moveToNext());
        }

        return light;
    }


    /*
     * getting light count
     */
    public int getLightOnCount() {
        String countQuery = "SELECT  * FROM " + TABLE_TIME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();

        // return count
        return count;
    }

    /*
     * Updating a light
     */
    public int updateLight(Light light) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(TABLE_NAME, light.getTime());

        // updating row
        return db.update(TABLE_TIME, values, TABLE_ID + " = ?",
                new String[] { String.valueOf(light.getId()) });
    }





}
